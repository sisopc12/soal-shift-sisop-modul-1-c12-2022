scriptDir=`pwd`
overwrite=$(date +"%Y%m%d%H%M%S")
mkdir -p ~/log

touch  $scriptDir/log/"metrics_$overwrite.log"

while sleep 60
do
echo -n "Mengeksekusi Perintah (tekan CTRL + C untuk menghentikan) pada : "; date;
echo "$(free -m)" >> $scriptDir/log/"metrics_$overwrite.log"
echo "$(du -sh $scriptdir)" >> $scriptDir/log/"metrics_$overwrite.log"
date >> $scriptDir/log/"metrics_$overwrite.log"
chmod 600 $scriptdir/log/"metrics_$overwrite.log"
done
