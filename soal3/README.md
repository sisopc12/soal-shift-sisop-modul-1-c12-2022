<h1>-----> Soal Nomor 3</h1>

3. Ubay sangat suka dengan komputernya. Suatu saat komputernya crash secara tiba-tiba :(. Tentu saja Ubay menggunakan linux. Akhirnya Ubay pergi ke tukang servis untuk memperbaiki laptopnya. Setelah selesai servis, ternyata biaya servis sangatlah mahal sehingga ia harus menggunakan dana kenakalannya untuk membayar biaya servis tersebut. Menurut Mas Tukang Servis, laptop Ubay overload sehingga mengakibatkan crash pada laptopnya. Karena tidak ingin hal serupa terulang, Ubay meminta kalian untuk membuat suatu program monitoring resource yang tersedia pada komputer.

Buatlah program monitoring resource pada komputer kalian. Cukup monitoring ram dan monitoring size suatu directory. Untuk ram gunakan command `free -m`. Untuk disk gunakan command `du -sh <target_path>`. Catat semua metrics yang didapatkan dari hasil `free -m`. Untuk hasil du -sh <target_path> catat size dari path directory tersebut. Untuk target_path yang akan dimonitor adalah /home/{user}/.

1. Masukkan semua metrics ke dalam suatu file log bernama metrics_{YmdHms}.log. {YmdHms} adalah waktu disaat file script bash kalian dijalankan. Misal dijalankan pada 2022-01-31 15:00:00, maka file log yang akan tergenerate adalah metrics_20220131150000.log.
2. Script untuk mencatat metrics diatas diharapkan dapat berjalan otomatis pada setiap menit.
3. Kemudian, buat satu script untuk membuat agregasi file log ke satuan jam. Script agregasi akan memiliki info dari file-file yang tergenerate tiap menit. Dalam hasil file agregasi tersebut, terdapat nilai minimum, maximum, dan rata-rata dari tiap-tiap metrics. File agregasi akan ditrigger untuk dijalankan setiap jam secara otomatis. Berikut contoh nama file hasil agregasi metrics_agg_2022013115.log dengan format metrics_agg_{YmdH}.log
4. Karena file log bersifat sensitif pastikan semua file log hanya dapat dibaca oleh user pemilik file.

Note:

- nama file untuk script per menit adalah minute_log.sh
- nama file untuk script agregasi per jam adalah aggregate_minutes_to_hourly_log.sh
- semua file log terletak di /home/{user}/log

Berikut adalah contoh isi dari file metrics yang dijalankan tiap menit:

mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size

15949,10067,308,588,5573,4974,2047,43,2004,/home/youruser/test/,74M

Berikut adalah contoh isi dari file aggregasi yang dijalankan tiap jam:

type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size

minimum,15949,10067,223,588,5339,4626,2047,43,1995,/home/user/test/,50M

maximum,15949,10387,308,622,5573,4974,2047,52,2004,/home/user/test/,74M

average,15949,10227,265.5,605,5456,4800,2047,47.5,1999.5,/home/user/test/,62

---->>>>>

a dan b

Command yang ingin dioutput adalah

bash
free -m
du -sh /home/{user}

Sehingga buatlah file menggunakan **nano** untuk **minute_log.sh**

lalu dalam file tersebut masukkan

echo "$(free -m)" >> $scriptDir/log/"metrics_$overwrite.log"
echo "$(du -sh $scriptdir)" >> $scriptDir/log/"metrics_$overwrite.log"

untuk mengoutput hasil command sebelumnya ke sebuah file bernama metrics_ + waktu berformat **YmdHs**.log

overwrite=$(date +"%Y%m%d%H%M%S")

untuk mempermudah membuat file langsung di direktori users dibuat

scriptDir=`pwd`

lalu karena diminta ada file "log" untuk menyimpan log script yang telah dijalankan

bash
scriptDir=`pwd`
overwrite=$(date +"%Y%m%d%H%M%S")
mkdir -p ~/log

karena dari soal diminta untuk dibuatkan log file jika belum ada di direktori user dibuatlah command membuat file tersebut
Karena file diminta untuk menjalankan script setiap menit, gunakan for loop dengan sleep 60 detik


while sleep 60
do
echo -n "Mengeksekusi Perintah (tekan CTRL + C untuk menghentikan) pada : "; date;
echo "$(free -m)" >> $scriptDir/log/"metrics_$overwrite.log"
echo "$(du -sh $scriptdir)" >> $scriptDir/log/"metrics_$overwrite.log"
date >> $scriptDir/log/"metrics_$overwrite.log"
chmod 600 $scriptdir/log/"metrics_$overwrite.log"
done

saya mengguankan loop while sleep berdasarkan referensi

https://www.cyberciti.biz/faq/linux-unix-sleep-bash-scripting/

d.

Agar file tidak dapat diakses, maka gunakan set permissions pada linux, lalu pastikan untuk setting Group dan Others dibuat akses menjadi none

Sayangnya, perintah tersebut hanya memastikan terkuncinya folder, tetapi filenya sendiri tidak terkunci sehingga kita harus menambahkan chmod untuk mengatur permission file

bash
chmod 600 $scriptdir/log/"metrics_$overwrite.log"

Dan line ini dimasukkan sebelum loop diulang kembali, sehingga menjadi seperti berikut

bash
scriptDir=`pwd`
overwrite=$(date +"%Y%m%d%H%M%S")
mkdir -p ~/log

touch  $scriptDir/log/"metrics_$overwrite.log"

while sleep 60
do
echo -n "Mengeksekusi Perintah (tekan CTRL + C untuk menghentikan) pada : "; date;
echo "$(free -m)" >> $scriptDir/log/"metrics_$overwrite.log"
echo "$(du -sh $scriptdir)" >> $scriptDir/log/"metrics_$overwrite.log"
date >> $scriptDir/log/"metrics_$overwrite.log"
chmod 600 $scriptdir/log/"metrics_$overwrite.log"
done


artinya setelah file digenerate akan di set permissionnya untuk user dengan value 6, group dengan value 0, dan user lain dengan value 0
(value 0 tidak memiliki akses dan value 6 memiliki akses RW)