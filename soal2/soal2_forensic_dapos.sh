#!/bin/bash

dirNow=`pwd`
folder=$dirNow/forensic_log_website_daffainfo_log
logfile=$dirNow/log_website_daffainfo.log

mkdir -p $folder

awk ' 
BEGIN{FS=":"}
	{gsub(/"/, "", $3)
	arr[$3]++}
	END {
		for (i in arr) {
			if (i != "Request"){
				n+=arr[i]
			}
		}
		avg=n/12 
		printf "Rata rata serangan perjam : %f", avg}
' $logfile > $folder/ratarata.txt


awk '
BEGIN{FS=":"}
	{gsub(/"/, "", $1)
	arr[$1]++}
	END {
		ip
		total=0
		for (i in arr){
			if (total < arr[i]){
				ip = i
				total = arr[ip]
			}
		}
		print "Pengakses server terbanyak: " ip "\nSebanyak: " total " request\n"}
' $logfile > $folder/result.txt

awk '
BEGIN{FS=":"}
	/curl/ {c++} 
	END {print "Terdapat " c " request dengan curl sebagai user-agent\n"}
' $logfile >> $folder/result.txt

awk '
BEGIN{FS=":"}
	{if($3=="02") printf "%s Jam 2 Pagi \n", $1}
' $logfile >> $folder/result.txt
