    <h1>SOAL 2</h1>
    <hr>
    <h3>membuat direktori</h3>
    <hr>
    dirNow=`pwd`
    folder=$dirNow/forensic_log_website_daffainfo_log
    logfile=$dirNow/log_website_daffainfo.log
    mkdir -p $folder
    <hr>
Melakukan pembuatan folder dengan  forensic_log_website_daffainfo_log dan melakukan storing alamat file.log.
Pada mkdir digunakan opsi -p untuk melakukan pembuatan folder hanya ketika folder tidak ditemukan.
    <h3>Rata-Rata Serangan Per-jam</h3>
    <hr>
    awk ' 
    BEGIN{FS=":"}
        {gsub(/"/, "", $3)
        arr[$3]++}
        END {
            for (i in arr) {
                if (i != "Request"){
                    n+=arr[i]
                }
            }
            avg=n/12 
            printf "Rata rata serangan perjam : %f", avg}
    ' $logfile > $folder/ratarata.txt
    <hr>
Pada perintah tersebut dilakukan perhitungan dengan cara melakukan definisi file separator, lalu meniadakan tanda "
lalu dilakukan storing jumlah muncul ip menggunakan array. Selanjutnya akan dihitung total kemunculan, lalu akan
dibagi dengan 12 karena serangan terjadi selama 12 jam.
    <hr>
    <h3>ip Pengakses Terbanyak</h3>
    <hr>
    awk '
    BEGIN{FS=":"}
        {gsub(/"/, "", $1)
        arr[$1]++}
        END {
            ip
            total=0
            for (i in arr){
                if (total < arr[i]){
                    ip = i
                    total = arr[ip]
                }
            }
            print "Pengakses server terbanyak: " ip "\nSebanyak: " total " request\n"}
    ' $logfile > $folder/result.txt
    <hr>
Pada perintah tersebut dilakukan perhitungan dengan cara melakukan definisi file separator, lalu meniadakan tanda "
lalu dilakukan storing jumlah muncul ip menggunakan array. Selanjutnya mencari array dengan nilai terbesar dan
dilakukan print ip array tersebut.
    <hr>
    <h3>Request dengan CURL</h3>
    <hr>
    awk '
    BEGIN{FS=":"}
        /curl/ {c++} 
        END {print "Terdapat " c " request dengan curl sebagai user-agent\n"}
    ' $logfile >> $folder/result.txt
    <hr>
Pada perintah tersebut akan dilakukan perhitungan jumlah ip dengan curl sebagai user-agent.
    <hr>
    <h3>ip Yang Melakukan Akses pada Jam 2</h3>
    <hr>
    awk '
    BEGIN{FS=":"}
        {if($3=="02") printf "%s Jam 2 Pagi \n", $1}
    ' $logfile >> $folder/result.txt
    <hr>
Pada perintah ini, akan dilakukan print pada ip yang melakukan akses pada jam 2 dengan cara
mendefinisikan separator ":" lalu dilakukan print ip dengan kondisi jika jam berisikan "02".