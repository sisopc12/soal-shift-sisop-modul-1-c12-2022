#!/bin/bash
do_check_password(){
	local lengthPassword=${#password}
	if [[ $lengthPassword -lt 8 ]]
	then
		echo "Password should be more then 8 characters!"

	else
		do_login
	fi
}
do_login(){
local encryptedPassword=`echo -n "$password"|base64`
	if grep -q "$username $encryptedPassword" "$locUser"
	then
		echo "$calendar $time LOGIN:INFO User $username logged in!" >> $locLog
		echo "Login Successfull"

		printf "Enter command (dl or att): "
		read command
		if [[ $command == dl ]]
		then
			do_dl_pic
		elif [[ $command == att ]]
		then
			do_att
		else
			echo "Command Error!"
		fi
	else
		failUserExist="Failed Login attempt for User:$username"
		failUserNotExist="Login Failed! No User:$username Found!"
		if ! grep -q "$username" "$locUser"
		then
			echo -e "\n\n$failUserNotExist"
			printf "\n\n-------DO REGISTER!!!\n\n"
			source $locationNow/register.sh
		elif grep -q "$username" "$locUser"
		then
			echo -e "\n\n$failUserExist"
			echo "$calendar $time LOGIN:ERROR $failUserExist" >> $locLog
		fi
	fi
}
do_dl_pic(){
	printf "Enter number of picture : "
	read n
	if [[ ! -f "$folder.zip" ]]
	then
		mkdir $folder
		count=0
		do_start_dl
	else
		do_unzip
	fi
}
do_unzip(){
	unzip -P $password $folder.zip
	rm $folder.zip

	count=$(find $folder -type f | wc -l)
	do_start_dl
}
do_start_dl(){
	for(( i = $count+1; i<=$n+$count; i++))
	do
		wget https://loremflickr.com/320/240 -O $folder/PIC_$i.jpg
	done

	zip --password $password -r $folder.zip $folder/
	rm -rf $folder
}
do_att(){
	printf "User attempt number :"
	awk -v name=$username '$0 ~ name && /LOGIN/{ ++n } END { print n }' log.txt
}

# main
calendar=$(date +%D)
time=$(date +%T)
printf "\n-------DO LOGIN!!!\n\n"
printf "Enter username : "
read username

printf "Enter password : "
read -s password

locationNow=`pwd`
if [[ ! -f "users.txt" ]]
then
	touch users.txt
fi
if [[ ! -f "log.txt" ]]
then
	touch log.txt
fi

folder=$(date +%Y-%m-%d)_$username
locLog=$locationNow/log.txt
locUser=$locationNow/users.txt

do_check_password

