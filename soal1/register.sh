do_check_password(){
 local lengthPassword=${#password}
 local locUser=$locationNow/users.txt
 local locLog=$locationNow/log.txt
 local encryptedPassword=`echo -n "$password"|base64`
	if grep -q $username "$locUser"
	then
	   existsUser="User Already Exists"

	   echo $existsUser
	   echo $calendar $time REGISTER:ERROR $existsUser >> $locLog

	elif [[ $password == $username ]]
	then
	   echo "Password couldn't match with username"

	elif [[ $lengthPassword -lt 8 ]]
	then
	   echo "Password should be more than 8 characters"

	elif [[ "$password" != *[[:upper:]]* || "$password" != *[[:lower:]]* || "$password" != *[0-9]* ]]
	then
	   echo "Password should contains at least upper, lower, and number!"

	else
	   printf "\n\nRegistered Successfully!"
	   echo $calendar $time REGISTER:INFO User $username registered successfully >> $locLog
	   echo $username $encryptedPassword >> $locUser
	   source `pwd`/main.sh
	fi

}

calendar=$(date +%D)
time=$(date +%T)

printf "Enter Username: "
read username

printf "Enter Password: "
read -s password

locationNow=`pwd`
if [[ ! -f "users.txt" ]]
then
	touch users.txt
fi
if [[ ! -f "log.txt" ]]
then
	touch log.txt
fi

do_check_password
