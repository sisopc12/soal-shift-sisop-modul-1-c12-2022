<h1>SOAL 1</h1>
    <hr>
    <h2>register.sh</h2>
    <hr>
    <h3>main</h3>
    <hr>
calendar=$(date +%D)
time=$(date +%T)

printf "Enter Username: "
read username

printf "Enter Password: "
read -s password

locationNow=`pwd`
if [[ ! -f "users.txt" ]]
then
	touch users.txt
fi
if [[ ! -f "log.txt" ]]
then
	touch log.txt
fi

do_check_password
<hr>
Pada main, dilakukan read untuk username dan password serta cek apakah ada users.txt dan log.txt. Jika tidak akan dibuat file tersebut.
<hr>
    <h3>fungsi</h3>
<hr>
do_check_password(){
    ...
}
<hr>
Pada fungsi do_check_password akan dilakukan pengecekan apakah password yang diinputkan sesuai dengan kriteria dan apakah username telah tersedia.
<hr>
<h2>main.sh</h2>
<hr>
<h3>main</h3>
<hr>
calendar=$(date +%D)
time=$(date +%T)
printf "\n-------DO LOGIN!!!\n\n"
printf "Enter username : "
read username

printf "Enter password : "
read -s password

locationNow=`pwd`
if [[ ! -f "users.txt" ]]
then
	touch users.txt
fi
if [[ ! -f "log.txt" ]]
then
	touch log.txt
fi

folder=$(date +%Y-%m-%d)_$username
locLog=$locationNow/log.txt
locUser=$locationNow/users.txt

do_check_password
<hr>
Pada main, dilakukan read untuk username dan password. Lalu akan dilakukan store lokasi log.txt dan users.txt.
<hr>
<h3>fungsi</h3>
<hr>
do_check_password(){
    ...
}
do_login(){
    ...
}
do_dl_pic(){
	...
}
do_unzip(){
	...
}
do_start_dl(){
	...
}
do_att(){
	...
}
<hr>
Pada fungsi do_check_password akan dilakukan pengecekan apakah password kurang dari 8 karakter atau sudah memenuhi.
Apabila terpenuhi, akan dilakukan pengecekan pada username dan password yang terenkripsi pada users.txt. Apabila match, maka
akan dilakukan  store info login pada log dan pemilihan menu dl atau att. Pada dl, akan dilakukan download gambar yang
dimasukan kedalam folder, lalu akan dilakukan kompresi pada gambar yang ada dengan format yang ditentukan.

Pada fungsi do_att, akan dilakukan perhitungan jumlah berapa kali user melakukan aktivitas pada log.